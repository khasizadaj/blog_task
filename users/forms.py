from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from .models import Profile


class UserRegisterForm(UserCreationForm):
    # additional fields that we added
    email = forms.EmailField()

    class Meta:
        model = User

        # we specified order of inut fields
        fields = ['username', 'email', 'password1', 'password2']


class UserUpdateForm(forms.ModelForm):
    # additional fields that we added
    email = forms.EmailField()

    class Meta:
        model = User

        # we specified order of inut fields
        fields = ['username', 'email']


class ProfileUpdateForm(forms.ModelForm):
    class Meta:
        model = Profile

        # we specified order of inut fields
        fields = ['picture'] 
