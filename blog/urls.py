from django.urls import path, include
from .views import (
  PostListView, 
  PostDetailView,
  PostCreateView,
  PostUpdateView,
  PostDeleteView,
  UserPostListView
)
from . import views

urlpatterns = [
  path('', PostListView.as_view(), name="home"),
  path('user/<str:username>/posts/', UserPostListView.as_view(), name="user_posts"),
  path('post/<int:pk>/', PostDetailView.as_view(), name="detail_view"),
  path('post/create/', PostCreateView.as_view(), name="create_view"),
  path('post/<int:pk>/update/', PostUpdateView.as_view(), name="update_view"),
  path('post/<int:pk>/delete/', PostDeleteView.as_view(), name="delete_view"),
  path('about/', views.about, name="about"),
]